package itis.parsing;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public interface ParkParsingService {

    Park parseParkData(String parkDatafilePath) throws ParkParsingException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException, IOException;

}
