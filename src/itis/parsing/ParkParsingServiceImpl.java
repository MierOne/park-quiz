package itis.parsing;
import itis.parsing.annotations.*;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException, IOException {
        ArrayList<String[]> arrayL = new ArrayList<>();
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(new File(parkDatafilePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        Park park = null;
        try {
            Constructor<Park> constructor = Park.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            park = constructor.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            bufferedReader.readLine();
            String line = bufferedReader.readLine();
            while(!line.equals("***")){
                String[] param = line.split(": ");
                if(param.length<2){
                    param = line.split(":");
                }
                param[0] = param[0].substring(1, param[0].length()-1);
                if(param[1].charAt(0)==('"')){
                    param[1] = param[1].substring(1, param[1].length()-1);
                }
                arrayL.add(param);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        bufferedReader.close();

        Field[] fields = park.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            if(fields[i].isAnnotationPresent(FieldName.class)){
                for (int j = 0; j < arrayL.size(); j++) {
                    Field field = fields[i];
                    field.setAccessible(true);
                    if (arrayL.get(j)[0].equals(field)){

                    }
                }
            }
            if(fields[i].isAnnotationPresent(MaxLength.class)){

            }
            if(fields[i].isAnnotationPresent(NotBlank.class)){
                for (int j = 0; j < arrayL.size(); j++) {
                    if(arrayL.get(j)[0].equals(fields[i].toString())){
                    }
                }
            }
        }

        return null;
    }
}
